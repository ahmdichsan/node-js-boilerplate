module.exports = {
  apps : [{
    name: 'server-service',
    script: 'dist/index.js',
    instances: 2,
    autorestart: true,
    // max_memory_restart: '1G',
    env_development: {
      NODE_ENV: 'development',
      watch: 'dist',
      watch_options: {
        followSymlinks: false,
      },
    },
    env_stagging: {
      NODE_ENV: 'staging',
      watch: 'dist',
      watch_options: {
        followSymlinks: false,
      },
    },
    env_production: {
      NODE_ENV: 'production',
      watch: 'dist',
      watch_options: {
        followSymlinks: false,
      },
    },
  }],
};
