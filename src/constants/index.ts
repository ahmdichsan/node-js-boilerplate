import { RouteOptions } from '@hapi/hapi';

export const uploadRouteOptions: RouteOptions = {
  payload: {
    output: 'stream',
    allow: 'multipart/form-data',
    parse: true,
    maxBytes: 1024 * 1024 * 1, // 1 MB
    multipart: {
      output: 'stream',
    },
  },
};
