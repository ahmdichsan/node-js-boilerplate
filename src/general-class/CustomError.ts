export class CustomError extends Error {
  callerFunction: string;

  constructor(message: string, callerFunction: string) {
    super();
    this.message = message;
    this.callerFunction = callerFunction;
    console.log(`error ${this.callerFunction}: ${this.message}`);
  }
}
