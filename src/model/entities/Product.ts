import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';
import { MandatoryField } from './MandatoryField';

@Entity()
export class Product extends MandatoryField {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({ type: 'varchar', length: 100 })
  name: string;

  @Column()
  price: number;

  @Column({ type: 'varchar', length: 100, nullable: true })
  productImageName: string;
}
