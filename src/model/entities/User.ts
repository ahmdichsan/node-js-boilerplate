import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';
import { MandatoryField } from './MandatoryField';

@Entity()
export class User extends MandatoryField {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({ type: 'varchar', length: 100 })
  name: string;

  @Column({ type: 'varchar', length: 100 })
  userName: string;

  @Column({ type: 'varchar', length: 100 })
  password: string;
}
