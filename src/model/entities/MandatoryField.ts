import { Column, CreateDateColumn, UpdateDateColumn } from 'typeorm';

export class MandatoryField {
  @Column({ type: 'varchar', length: 100 })
  createdBy: string;

  @CreateDateColumn()
  createdDate: Date;

  @Column({ type: 'varchar', length: 100, nullable: true })
  lastUpdatedBy: string;

  @UpdateDateColumn({ nullable: true })
  lastUpdatedDate: Date;

  @Column()
  activeFlag: boolean;
}
