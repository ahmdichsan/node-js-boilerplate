import { Entity, PrimaryGeneratedColumn, ManyToOne, JoinColumn, Column } from 'typeorm';
import { MandatoryField } from './MandatoryField';
import { User } from './User';

@Entity()
export class ToDoUser extends MandatoryField {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({ name: 'toDo', type: 'varchar' })
  toDo: string;

  @Column({ name: 'isToDoDone', type: 'bool', default: false })
  isToDoDone: boolean;

  @ManyToOne(() => User, user => user.id)
  @JoinColumn({ name: 'userId' })
  userId: User;
}
