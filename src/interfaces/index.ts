import { Request, Util } from "@hapi/hapi";

export declare interface DefaultHeaders {
  authorization?: string;
  information?: string;
}

export declare interface RequestObject<
  Payload = any,
  Params = Util.Dictionary<string>,
  Headers = DefaultHeaders
> extends Omit<Request, 'payload' | 'params' | 'headers'> {
  payload: Payload;
  params: Params;
  headers: Headers;
}

export declare interface Decoded {
  agent: { userName: string, password: string }
}