import { getConnection } from 'typeorm';
import { ToDoUser } from '../../../model/entities';

export async function getToDoUserById(id: string): Promise<ToDoUser | undefined> {
  return await getConnection().getRepository(ToDoUser).findOne({ id });
}

export async function getAllActiveTodosUser(): Promise<ToDoUser[]> {
  return await getConnection().getRepository(ToDoUser).find({
    where: { activeFlag: true },
    order: { createdDate: 'ASC' },
  });
}

export async function getAllToDosUser(): Promise<ToDoUser[]> {
  return await getConnection().getRepository(ToDoUser).find({
    order: { createdDate: 'ASC' },
  });
}

export async function getToDosUserByUserId(userId: string): Promise<ToDoUser[]> {
  return await getConnection().getRepository(ToDoUser).find({
    where: { userId: { id: userId } },
    order: { createdDate: 'ASC' },
  });
}