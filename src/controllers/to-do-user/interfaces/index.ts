export declare interface CreateToDoUserPayload {
  id: string;
  toDo: string;
  createdDate?: Date;
  lastUpdatedDate?: Date;
}
