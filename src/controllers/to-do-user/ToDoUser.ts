import { ResponseToolkit, ResponseObject } from '@hapi/hapi';
import { getConnection } from 'typeorm';
import { plainToClass } from 'class-transformer';
import validator from 'validator';

import { User, ToDoUser } from '../../model/entities';

import { RequestObject } from '../../interfaces';
import { CreateToDoUserPayload } from './interfaces';

import { getToDoUserById, getAllToDosUser, getToDosUserByUserId } from './utils';
import { userParser } from '../../utils';

class ToDoUserController {
  /**
  * @param req toDo data
  * @description controller to create toDo user
  */
  static createToDoUser = async (req: RequestObject<ToDoUser>, res: ResponseToolkit): Promise<object> => {
    try {
      const userInfo = userParser(req.headers.authorization || '');
      const { userName } = userInfo;

      const { id, toDo, createdDate } = req.payload;

      if (id && !validator.isUUID(id)) return res.response('Invalid UUID').code(400);

      const today = new Date();

      const userClass = plainToClass(User, userInfo);

      const toDoUser = new ToDoUser();

      const toDoUserById = id ? await getToDoUserById(id) : undefined;

      if (!toDoUserById) toDoUser.id = id;
      toDoUser.activeFlag = true;
      toDoUser.createdBy = userName;
      toDoUser.createdDate = createdDate || today;
      toDoUser.toDo = toDo;
      toDoUser.userId = userClass;

      await getConnection().getRepository(ToDoUser).save(toDoUser);

      delete toDoUser.userId.password;
      return res.response(toDoUser).code(200);
    } catch (error) {
      const errorMessage = `error save ToDo: ${error.message}`;
      console.log(errorMessage);
      return res.response(errorMessage).code(500);
    }
  };

  /**
  * @param res return all todo user
  * @description controller to return all todo user
  */
  static allToDosUser = async (req: RequestObject, res: ResponseToolkit): Promise<ResponseObject> => {
    try {
      const allToDosUser = await getAllToDosUser();

      return res.response(allToDosUser).code(200);
    } catch (error) {
      const errorMessage = `error allToDosUser: ${error.message}`;
      console.log(errorMessage);
      return res.response(errorMessage).code(500);
    }
  };

  /**
  * @param res return todo by id
  * @description controller to return todo by id
  */
  static toDoUserById = async (req: RequestObject, res: ResponseToolkit): Promise<ResponseObject> => {
    try {
      const id = req.params.id;
      const toDoUserById = await getToDoUserById(id);

      if (!toDoUserById) return res.response(`ToDo user with id ${id} not exist`).code(400);

      return res.response(toDoUserById).code(200);
    } catch (error) {
      const errorMessage = `error toDoUserById: ${error.message}`;
      console.log(errorMessage);
      return res.response(errorMessage).code(500);
    }
  };

  /**
  * @param req todo data
  * @description controller to update todo, this API not used yet and still has flaws
  */
  static updateToDoUser = async (req: RequestObject<CreateToDoUserPayload>, res: ResponseToolkit): Promise<object> => {
    try {
      const id = req.params.id;
      const toDoUserById = await getToDoUserById(id);

      if (!toDoUserById) return res.response(`ToDo user with id ${id} not exist`).code(400);

      const today = new Date();

      const { userName } = userParser(req.headers.authorization || '');

      toDoUserById.lastUpdatedBy = userName;
      toDoUserById.lastUpdatedDate = req.payload.lastUpdatedDate || today;
      toDoUserById.toDo = req.payload.toDo;

      const toDoUserByIdClass = plainToClass(ToDoUser, toDoUserById);

      await getConnection().getRepository(ToDoUser).update(id, toDoUserByIdClass);

      return res.response('ToDo User Successfully Updated').code(200);
    } catch (error) {
      const errorMessage = `error update todo user: ${error.message}`;
      console.log(errorMessage);
      return res.response(errorMessage).code(500);
    }
  };

  /**
  * @param re1 todo by id
  * @description controller to delete todo by id
  */
  static deleteToDoUserById = async (req: RequestObject, res: ResponseToolkit): Promise<ResponseObject> => {
    try {
      const id = req.params.id;
      const toDoById = await getToDoUserById(id);

      if (!toDoById) return res.response(`ToDo user with id ${id} not exist`).code(400);

      await getConnection().getRepository(ToDoUser).delete(id);

      return res.response('ToDo User Successfully Deleted').code(200);
    } catch (error) {
      const errorMessage = `error delete todo user by id: ${error.message}`;
      console.log(errorMessage);
      return res.response(errorMessage).code(500);
    }
  };

  /**
  * @param req toDo data
  * @description controller to update toDo user
  */
  static updateToDoUserStatus = async (req: RequestObject<ToDoUser>, res: ResponseToolkit): Promise<object> => {
    try {
      const toDoId = req.params.toDoId;

      if (!validator.isUUID(toDoId)) return res.response('Invalid To Do id').code(400);

      const userInfo = userParser(req.headers.authorization || '');
      const { userName } = userInfo;

      const { toDo, createdDate, lastUpdatedDate, isToDoDone } = req.payload;

      const message = isToDoDone ? 'Horray, this ToDo is Done!' : 'Alrite, lets get this ToDo done!';

      const today = new Date();

      const toDoUserById = await getToDoUserById(toDoId);

      if (!toDoUserById) {
        const userClass = plainToClass(User, userInfo);

        const toDoUser = new ToDoUser();

        toDoUser.id = toDoId;
        toDoUser.activeFlag = true;
        toDoUser.createdBy = userName;
        toDoUser.createdDate = createdDate || today;
        toDoUser.lastUpdatedBy = userName;
        toDoUser.lastUpdatedDate = lastUpdatedDate || today;
        toDoUser.toDo = toDo;
        toDoUser.userId = userClass;
        toDoUser.isToDoDone = isToDoDone;

        await getConnection().getRepository(ToDoUser).save(toDoUser);
        return res.response(message).code(200);
      }

      toDoUserById.isToDoDone = isToDoDone;
      toDoUserById.lastUpdatedBy = userName;
      toDoUserById.lastUpdatedDate = lastUpdatedDate || today;

      await getConnection().getRepository(ToDoUser).update(toDoId, toDoUserById);

      return res.response(message).code(200);
    } catch (error) {
      const errorMessage = `error update updateToDoUserStatus: ${error.message}`;
      console.log(errorMessage);
      return res.response(errorMessage).code(500);
    }
  };

  /**
  * @param res return todo by user id
  * @description controller to return todo by user id
  */
  static toDosUserByUserId = async (req: RequestObject, res: ResponseToolkit): Promise<ResponseObject> => {
    try {
      const { id } = userParser(req.headers.authorization || '');

      const toDosUserByUserId = await getToDosUserByUserId(id);

      return res.response(toDosUserByUserId).code(200);
    } catch (error) {
      const errorMessage = `error toDosUserByUserId: ${error.message}`;
      console.log(errorMessage);
      return res.response(errorMessage).code(500);
    }
  };
}

export default ToDoUserController;
