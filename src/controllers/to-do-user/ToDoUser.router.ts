import { ServerRoute, ResponseToolkit } from '@hapi/hapi';
import ToDoUser from './ToDoUser';
import { RequestObject } from '../../interfaces';

const Routes: ServerRoute[] = [
  {
    method: 'POST',
    path: '/todo',
    handler: (req: RequestObject, res: ResponseToolkit): object => ToDoUser.createToDoUser(req, res),
  },
  {
    method: 'GET',
    path: '/todo',
    handler: (req: RequestObject, res: ResponseToolkit): object => ToDoUser.allToDosUser(req, res),
  },
  {
    method: 'GET',
    path: '/todo/{id}',
    handler: (req: RequestObject, res: ResponseToolkit): object => ToDoUser.toDoUserById(req, res),
  },
  {
    method: 'PUT',
    path: '/todo/{id}',
    handler: (req: RequestObject, res: ResponseToolkit): object => ToDoUser.updateToDoUser(req, res),
  },
  {
    method: 'DELETE',
    path: '/todo/{id}',
    handler: (req: RequestObject, res: ResponseToolkit): object => ToDoUser.deleteToDoUserById(req, res),
  },
  {
    method: 'PUT',
    path: '/todo/{toDoId}/toggle-status',
    handler: (req: RequestObject, res: ResponseToolkit): object => ToDoUser.updateToDoUserStatus(req, res),
  },
  {
    method: 'GET',
    path: '/todo/user',
    handler: (req: RequestObject, res: ResponseToolkit): object => ToDoUser.toDosUserByUserId(req, res),
  },
];

export default Routes;
