import { ServerRoute, ResponseToolkit } from '@hapi/hapi';
import Product from './Product';
import { RequestObject } from '../../interfaces';
import { uploadRouteOptions } from '../../constants';

const Routes: ServerRoute[] = [
  {
    method: 'GET',
    path: '/products',
    handler: (req: RequestObject, res: ResponseToolkit): object => Product.allProducts(req, res),
  },
  {
    method: 'POST',
    path: '/products',
    handler: (req: RequestObject, res: ResponseToolkit): object => Product.createProduct(req, res),
    options: { ...uploadRouteOptions },
  },
  {
    method: 'PUT',
    path: '/products/{id}',
    handler: (req: RequestObject, res: ResponseToolkit): object => Product.updateProduct(req, res),
    options: { ...uploadRouteOptions },
  },
  {
    method: 'GET',
    path: '/products/{id}',
    handler: (req: RequestObject, res: ResponseToolkit): object => Product.productById(req, res),
  },
  {
    method: 'DELETE',
    path: '/products/{id}',
    handler: (req: RequestObject, res: ResponseToolkit): object => Product.deleteProductById(req, res),
  },
];

export default Routes;
