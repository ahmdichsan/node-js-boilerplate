import { ResponseToolkit, ResponseObject } from '@hapi/hapi';
import { getConnection } from 'typeorm';
import { plainToClass } from 'class-transformer';
import { Product } from '../../model/entities';
import { RequestObject } from '../../interfaces';
import { CreateProductPayload, ProductFullInfo } from './interfaces';
import { getAllProducts, getProductById, findProductImageNameInAnotherProductId } from './utils';
import { uploadFile, deleteFile, getSharedLink, userParser } from '../../utils';

class ProductController {
  /**
  * @param res return all products
  * @description controller to return all products
  */
  static allProducts = async (req: RequestObject, res: ResponseToolkit): Promise<ResponseObject> => {
    try {
      const allProducts = await getAllProducts();
      let productFullInfo: ProductFullInfo[] = [];

      for (let i = 0; i < allProducts.length; i += 1) {
        productFullInfo = [...productFullInfo, { ...allProducts[i], productImageLink: '' }];

        if (!allProducts[i].productImageName) continue;

        const sharedLink = await getSharedLink(allProducts[i].productImageName);

        const productImageLink = sharedLink.link;

        productFullInfo[i].productImageLink = productImageLink;
      }

      return res.response(productFullInfo).code(200);
    } catch (error) {
      const errorMessage = `error allProducts: ${error.message}`;
      console.log(errorMessage);
      return res.response(errorMessage).code(500);
    }
  };

  /**
  * @param res return product by id
  * @description controller to return product by id
  */
  static productById = async (req: RequestObject, res: ResponseToolkit): Promise<ResponseObject> => {
    try {
      const id = req.params.id;
      const productById = await getProductById(id);

      let productFullInfo: ProductFullInfo | null = null;

      if (!productById) return res.response(`Product with id ${id} not exist`).code(400);

      let productImageLink = '';

      if (productById.productImageName) {
        const sharedLink = await getSharedLink(productById.productImageName);
        productImageLink = sharedLink.link;
      }

      productFullInfo = {
        ...productById,
        productImageLink,
      };

      return res.response(productFullInfo).code(200);
    } catch (error) {
      const errorMessage = `error allProducts: ${error.message}`;
      console.log(errorMessage);
      return res.response(errorMessage).code(500);
    }
  };

  /**
  * @param req product data
  * @description controller to create product
  */
  static createProduct = async (req: RequestObject<CreateProductPayload>, res: ResponseToolkit): Promise<object> => {
    const queryRunner = getConnection().createQueryRunner();
    try {
      const product = JSON.parse(req.payload.product) as Product;
      const productFile = req.payload.productFile;

      let contents = null;
      let fileName = '';

      if (productFile) {
        contents = productFile._data;
        fileName = productFile.hapi.filename;
        product.productImageName = fileName;
      }

      const { userName } = userParser(req.headers.authorization || '');

      product.createdBy = userName;
      product.createdDate = new Date();
      const productClass = plainToClass(Product, product);

      await queryRunner.connect();
      await queryRunner.startTransaction();

      await queryRunner.manager.save(productClass);

      if (fileName && contents) await uploadFile(fileName, contents);

      await queryRunner.commitTransaction();
      await queryRunner.release();

      return res.response('Product Data Successfully Saved!').code(200);
    } catch (error) {
      await queryRunner.rollbackTransaction();
      await queryRunner.release();

      const errorMessage = `error save product: ${error.message}`;
      console.log(errorMessage);
      return res.response(errorMessage).code(500);
    }
  };

  /**
  * @param req product data
  * @description controller to update product
  */
  static updateProduct = async (req: RequestObject<CreateProductPayload>, res: ResponseToolkit): Promise<object> => {
    const queryRunner = getConnection().createQueryRunner();
    try {
      const id = req.params.id;
      const productById = await getProductById(id);

      if (!productById) return res.response(`Product with id ${id} not exist`).code(400);

      const productPayload = JSON.parse(req.payload.product) as Product;
      const productFile = req.payload.productFile;

      let contents = null;
      let fileName = '';
      let previousFileName = '';

      let message = 'Data Successfully Updated!';

      if (productById.productImageName) previousFileName = productById.productImageName;

      if (productFile) {
        contents = productFile._data;
        fileName = productFile.hapi.filename;
        productPayload.productImageName = fileName;
      }

      const { userName } = userParser(req.headers.authorization || '');

      const product: Product = {
        ...productById,
        ...productPayload,
        lastUpdatedDate: new Date(),
        lastUpdatedBy: userName,
      };

      await queryRunner.connect();
      await queryRunner.startTransaction();

      const productClass = plainToClass(Product, product);

      await queryRunner.manager.save(productClass);

      if (fileName && contents) {
        await uploadFile(fileName, contents);

        if (previousFileName) {
          const isProductFileNameUsed = await findProductImageNameInAnotherProductId(productPayload.id, previousFileName);

          try {
            // make sure if the image belongs not more than one id
            if (!isProductFileNameUsed) await deleteFile(previousFileName);
          } catch (error) {
            message = `previous image ${previousFileName} failed to be deleted: ${error.message}`;
          }
        }
      }

      await queryRunner.commitTransaction();
      await queryRunner.release();

      return res.response(message).code(200);
    } catch (error) {
      await queryRunner.rollbackTransaction();
      await queryRunner.release();

      const errorMessage = `error update product: ${error.message}`;
      console.log(errorMessage);
      return res.response(errorMessage).code(500);
    }
  };

  /**
  * @param re1 product by id
  * @description controller to delete product by id
  */
  static deleteProductById = async (req: RequestObject, res: ResponseToolkit): Promise<ResponseObject> => {
    try {
      const id = req.params.id;
      const productById = await getProductById(id);

      if (!productById) return res.response(`Product with id ${id} not exist`).code(400);

      if (productById.productImageName) {
        await deleteFile(productById.productImageName);
      }

      await getConnection().getRepository(Product).delete(id);

      return res.response('Data Successfully Deleted').code(200);
    } catch (error) {
      const errorMessage = `error deleteProductById: ${error.message}`;
      console.log(errorMessage);
      return res.response(errorMessage).code(500);
    }
  };
}

export default ProductController;
