import { Product } from "../../../model/entities";

export declare interface CreateProductPayload {
  product: string;
  productFile: any;
}

export declare interface ProductFullInfo extends Product {
  productImageLink: string;
}
