import { getConnection, Not } from 'typeorm';
import { Product } from '../../../model/entities';

export async function getProductById(id: string): Promise<Product | undefined> {
  return await getConnection().getRepository(Product).findOne({ id });
}

export async function getAllActiveProducts(): Promise<Product[]> {
  return await getConnection().getRepository(Product).find({
    where: { activeFlag: true },
    order: { createdDate: 'DESC' },
  });
}

export async function getAllProducts(): Promise<Product[]> {
  return await getConnection().getRepository(Product).find({
    order: { createdDate: 'DESC' },
  });
}

export async function findProductImageNameInAnotherProductId(id: string, productImageName: string): Promise<Product | undefined> {
  return await getConnection().getRepository(Product).findOne({
    where: { id: Not(id), productImageName },
  });
}
