import { getConnection } from 'typeorm';
import { User } from '../../../model/entities';

export async function getUserById(id: string): Promise<User | undefined> {
  return await getConnection().getRepository(User).findOne({ id });
}

export async function getAllActiveUsers(): Promise<User[]> {
  return await getConnection().getRepository(User).find({
    select: ['id', 'name', 'userName'],
    where: { activeFlag: true },
    order: { createdDate: 'DESC' },
  });
}

export async function getAllUsers(): Promise<User[]> {
  return await getConnection().getRepository(User).find({
    select: ['id', 'name', 'userName', 'activeFlag'],
    order: { createdDate: 'DESC' },
  });
}
