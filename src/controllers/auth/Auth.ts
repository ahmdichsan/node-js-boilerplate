import { ResponseToolkit, ResponseObject } from '@hapi/hapi';
import { getConnection } from 'typeorm';
import { plainToClass } from 'class-transformer';
import { User } from '../../model/entities';
import { RequestObject } from '../../interfaces';
import { getAllUsers, getUserById } from './utils';
import { sha256, generateToken, getExpiredTimePassword } from '../../utils';

class AuthController {
  /**
   * @param res return user data
   * @param req username and password
   * @description controller to do login user
   */
  static login = async (req: RequestObject, res: ResponseToolkit): Promise<ResponseObject> => {
    try {
      const userName: string = req.payload.userName;
      const password: string = sha256(req.headers.information || '');

      const findUser = await getConnection().getRepository(User).findOne({
        userName, password,
      });

      if (!findUser) return res.response('Wrong combination username and password').code(400);

      const exp = getExpiredTimePassword();
      const token = generateToken(findUser, exp);

      delete findUser.password;

      return res.response(findUser).header('content', token).header('exp', exp.toString()).code(200);
    } catch (error) {
      const errorMessage = `error login: ${error.message}`;
      console.log(errorMessage);
      return res.response(errorMessage).code(500);
    }
  };

  /**
   * @param req user object
   * @description controller to create user
   */
  static createUser = async (req: RequestObject<User>, res: ResponseToolkit): Promise<ResponseObject> => {
    const queryRunner = getConnection().createQueryRunner();
    try {
      const userData = req.payload;

      userData.password = sha256(userData.password);
      const userClass = plainToClass(User, userData);

      await queryRunner.connect();
      await queryRunner.startTransaction();

      await queryRunner.manager.save(userClass);

      await queryRunner.commitTransaction();
      await queryRunner.release();

      return res.response('User created!').code(200);
    } catch (error) {
      await queryRunner.rollbackTransaction();
      await queryRunner.release();

      const errorMessage = `error createUser: ${error.message}`;
      console.log(errorMessage);
      return res.response(errorMessage).code(500);
    }
  };

  /**
   * @param req user object
   * @description controller to update user
   */
  static updateUser = async (req: RequestObject<User>, res: ResponseToolkit): Promise<ResponseObject> => {
    const queryRunner = getConnection().createQueryRunner();
    try {
      const userData = req.payload;
      const userId = req.params.id;

      const userById = await getUserById(userId);

      if (!userById) return res.response('User not found!').code(400);

      if (userData.password) userData.password = sha256(userData.password);

      const userClass = plainToClass(User, { ...userById, ...userData, lastUpdatedDate: new Date(), lastUpdatedBy: 'system' });

      await queryRunner.connect();
      await queryRunner.startTransaction();

      await queryRunner.manager.save(userClass);

      await queryRunner.commitTransaction();
      await queryRunner.release();

      return res.response('User updated!').code(200);
    } catch (error) {
      await queryRunner.rollbackTransaction();
      await queryRunner.release();

      const errorMessage = `error createUser: ${error.message}`;
      console.log(errorMessage);
      return res.response(errorMessage).code(500);
    }
  };

  /**
   * @param res return all user
   * @description controller to return all user
   */
  static allUsers = async (req: RequestObject, res: ResponseToolkit): Promise<ResponseObject> => {
    try {
      const allUsers = await getAllUsers();

      return res.response(allUsers).code(200);
    } catch (error) {
      const errorMessage = `error allUsers: ${error.message}`;
      console.log(errorMessage);
      return res.response(errorMessage).code(500);
    }
  };
}

export default AuthController;
