import { ServerRoute, ResponseToolkit, ResponseObject } from '@hapi/hapi';
import Auth from './Auth';
import { RequestObject } from '../../interfaces';

const Routes: ServerRoute[] = [
  {
    method: 'GET',
    path: '/users',
    handler: (req: RequestObject, res: ResponseToolkit): Promise<ResponseObject> => Auth.allUsers(req, res),
    options: {
      auth: false,
    },
  },
  {
    method: 'POST',
    path: '/users',
    handler: (req: RequestObject, res: ResponseToolkit): Promise<ResponseObject> => Auth.createUser(req, res),
    options: {
      auth: false,
    },
  },
  {
    method: 'PUT',
    path: '/users/{id}',
    handler: (req: RequestObject, res: ResponseToolkit): Promise<ResponseObject> => Auth.updateUser(req, res),
    options: {
      auth: false,
    },
  },
  {
    method: 'POST',
    path: '/login',
    handler: (req: RequestObject, res: ResponseToolkit): Promise<ResponseObject> => Auth.login(req, res),
    options: {
      auth: false,
    },
  },
];

export default Routes;
