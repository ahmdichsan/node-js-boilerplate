import { getConnection } from 'typeorm';
import { User } from '../model/entities';

async function validate(decoded: { agent: { userName: string, password: string } }): Promise<object> {
  try {
    const user = await getConnection().getRepository(User).findOne({
      userName: decoded.agent.userName,
      password: decoded.agent.password,
    });

    if (user) return { isValid: true };

    return { isValid: false };
  } catch (error) {
    console.log(`error validate auth: ${error.message}`);
    return { isValid: false };
  }
}

const verifyOptions = {
  algorithms: [
    'HS256',
  ],
};

const options = {
  key: process.env.JWTSECRET,
  validate,
  verifyOptions,
};

export const STRATEGYNAME = 'jwt';
export const STRATEGYSCHEME = 'jwt';
export const STRATEGYOPTIONS = options;
