
import Plugins from './Plugins';
import Router from './Routes';
import Cors from './Cors';

export * from './AuthStrategy';
export {
  Plugins,
  Router,
  Cors,
};
