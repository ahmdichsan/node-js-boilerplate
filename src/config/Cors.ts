export default {
  origin: ['*'], // how to make this configurable?
  headers: [
    'Origin',
    'X-Requested-With',
    'Content-Type',
    'Accept',
    'Authorization',
    'responseType',
    'post',
    'information',
  ],
  exposedHeaders: [
    'Accept',
    'Content-Type',
    'Content-Disposition',
    'content',
    'exp',
  ],
  maxAge: 60,
  credentials: true,
};
