import routerProduct from '../controllers/product/Product.router';
import routerAuth from '../controllers/auth/Auth.router';
import routerToDoUser from '../controllers/to-do-user/ToDoUser.router';

export default [
  ...routerProduct,
  ...routerAuth,
  ...routerToDoUser,
];
