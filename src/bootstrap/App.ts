import * as Hapi from '@hapi/hapi';
import 'dotenv/config';
import 'reflect-metadata';
import { createConnection } from 'typeorm';
import { Plugins, Router, Cors, STRATEGYNAME, STRATEGYSCHEME, STRATEGYOPTIONS } from '../config';

async function App(): Promise<void> {
  const server = new Hapi.Server({
    port: process.env.PORT || 80,
    host: process.env.HOST || '0.0.0.0',
    routes: { 
      cors: Cors,
    },
  });

  await server.register(Plugins);

  server.auth.strategy(STRATEGYNAME, STRATEGYSCHEME, STRATEGYOPTIONS);
  server.auth.default(STRATEGYNAME);
  server.route(Router);

  const connection = await createConnection();
  await connection.synchronize();

  await server.start();
  console.log(`\nServer Service running at: ${server.info.uri}\n\n`);
}

export default App;
