import { decodeToken } from './HashEncrypt';
import { User } from '../model/entities';
import { CustomError } from '../general-class';
import { Decoded } from '../interfaces';

export * from './Dropbox';
export * from './Utils';
export * from './HashEncrypt';

export function getExpiredTimePassword(): number {
  return Math.floor(new Date().getTime() / 1000) + 90 * 24 * 60 * 60; // 90 day
}

export function userParser(authorization: string): User {
  try {
    const token = authorization.replace(/Bearer /g, '');
    const decoded = decodeToken(token) as Decoded;
    const authData = decoded.agent as User;
    return authData;
  } catch (error) {
    throw new CustomError(error.message, 'userParser');
  }
}
