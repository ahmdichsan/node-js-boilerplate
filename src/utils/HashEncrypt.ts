import 'dotenv/config';
import * as jwt from 'jsonwebtoken';
import * as Crypto from 'crypto-js';
import { getExpiredTimePassword } from '.';

export function sha256(message: string): string {
  return Crypto.SHA256(message).toString();
}

export function uid(): string {
  return sha256((new Date()).valueOf().toString());
}

export function generateToken(agent: object, exp: number | null = null): string {
  if (!exp) exp = getExpiredTimePassword();

  const secret: string = process.env.JWTSECRET || '';
  return jwt.sign({
    id: uid,
    agent,
    exp,
  }, secret);
}

export function decodeToken(token: string): string | object {
  const secret: string = process.env.JWTSECRET || '';
  return jwt.verify(token, secret);
}
