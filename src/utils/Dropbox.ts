import { Dropbox } from 'dropbox';
import * as fetch from 'isomorphic-fetch';
import 'dotenv/config';

export async function uploadFile(fileName: string, contents: Buffer): Promise<DropboxTypes.files.FileMetadata> {
  const accessToken = process.env.DROPBOX_TOKEN || '';
  const dbx = new Dropbox({ accessToken, fetch });

  return dbx.filesUpload({ path: `/${fileName}`, contents })
    .then()
    .catch(error => {
      let errorMessage = typeof error.error === 'string' ? error.error : error.error.error.path['.tag'];
      errorMessage = errorMessage || JSON.stringify(error);
      throw new Error(errorMessage);
    });
}

export async function getSharedLink(fileName: string): Promise<DropboxTypes.files.GetTemporaryLinkResult> {
  const accessToken = process.env.DROPBOX_TOKEN || '';
  const dbx = new Dropbox({ accessToken, fetch });

  return dbx.filesGetTemporaryLink({ path: `/${fileName}` })
    .then()
    .catch(error => {
      let errorMessage = typeof error.error === 'string' ? error.error : error.error.error.path['.tag'];
      errorMessage = errorMessage || JSON.stringify(error);
      throw new Error(errorMessage);
    });
}

export async function deleteFile(fileName: string): Promise<DropboxTypes.files.DeleteResult> {
  const accessToken = process.env.DROPBOX_TOKEN || '';
  const dbx = new Dropbox({ accessToken, fetch });

  return dbx.filesDeleteV2({ path: `/${fileName}` })
    .then()
    .catch(error => {
      let errorMessage = typeof error.error === 'string' ? error.error : error.error.error.path['.tag'];
      errorMessage = errorMessage || JSON.stringify(error);
      throw new Error(errorMessage);
    });
}
