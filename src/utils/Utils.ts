export function isDateValid(value: string): boolean {
  const date = new Date(value);
  if (Object.prototype.toString.call(date) === "[object Date]" && !isNaN(date.getTime())) return true;
  return false;
}

export function isIterable(object: any): boolean {
  return object != null && typeof object[Symbol.iterator] === 'function';
}

export function isValueNumber(value: any): boolean {
  return !isNaN(value - parseFloat(value));
}

export function isPasswordValid(password: string): boolean {
  const regex = /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d!$%@#£€*?&]{8,}$/;
  return regex.test(password);
}

export function getExtention(string: string): string {
  switch (string) {
    case 'image/jpeg':
      return 'jpeg';

    case 'image/jpg':
      return 'jpg';

    case 'image/png':
      return 'png';

    case 'image/svg+xml':
      return 'svg';

    case 'application/pdf':
      return 'pdf';

    default:
      return '';
  }
}
