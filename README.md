## Table of Contents

1.  [Available Scripts](#available-scripts)
    1. [`npm run serve`](#npm-run-serve)
    2. [`npm start`](#npm-start)
    3. [`npm run lint:dev`](#npm-run-lint:dev)
    4. [`npm run lint`](#npm-run-lint)
    5. [`npm test`](#npm-test)
    6. [`npm run coverage`](#npm-run-coverage)
    7. [`npm run build`](#npm-run-build)
    8. [`npm run eject`](#npm-run-eject)
2.  [Project Description](#project-description)
    1. [Try to use Hook](#try-to-use-hook)
    2. [Rules of Hook](#rules-of-hook)
    3. [Advice for Coding Style Guidline? Here are some references](#advice-for-coding-style-guidline?-here-are-some-references)
    4. [Folder Structure](#folder-structure)

## Stack

1. NodeJS with [HapiJS Framework](https://hapi.dev)
2. PostgresSQL
3. Dropbox for Static File Storage. See documentation [here](https://www.dropbox.com/developers/documentation/http/documentation)

## Available Scripts

In the project directory, you can run:

### `npm run serve`

This command use for custom run apps which executed in herokuapp. This command use in file named Procfile in the root folder of this project.

### `npm start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `npm run lint:dev`

This command run to check for eslint error and auto fix it. This is why this command suppose to run only in development.<br />
Eslint checking in CI/CD - ```.gitlab-ci.yml``` - run without auto fix. So it will break the deployment process if found code that violate eslint rules.

### `npm run lint`

This command run to check for eslint error without auto fix it and will caused error if found any violation to eslint rules.

### `npm test`

Launches the test runner in the interactive watch mode.<br />
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run coverage`

Launches the test runner and give coverage result at the end of the test.

### `npm run build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Project Description

### Try to use Hook

Since React Hooks are stable in React 16.8 and this project is created with React 16.12, then I decide to create this project with React Hook.<br />
Try React Hooks [here](https://reactjs.org/docs/hooks-intro.html).

### Rules of Hook

https://reactjs.org/docs/hooks-rules.html#explanation

### Advice for Coding Style Guidline? Here are some references

1. https://github.com/airbnb/javascript/tree/master/react
2. https://github.com/typescript-cheatsheets/react-typescript-cheatsheet

### Folder Structure
```
├── .vscode
├── bin
├── node_modules
├── public
├── src
│   ├── app - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - # Root Application
│   │   ├── App.reducers.ts
│   │   ├── App.sagas.ts
│   │   ├── App.store.ts
│   │   ├── App.test.tsx
│   │   └── App.tsx
│   ├── assets
│   │   ├── img
│   │   └── icon
│   ├── components
│   │   ├── your-shared-component-folder
│   │   │   ├── index.ts
│   │   │   ├── YourSharedComponent.scss
│   │   │   ├── YourSharedComponent.test.tsx
│   │   │   └── YourSharedComponent.tsx
│   │   └── index.ts
│   ├── config
│   │   ├── Config.persist.ts
│   │   ├── Config.reducers.ts
│   │   ├── Config.sagas.ts
│   │   ├── index.ts
│   │   └── Routes.ts
│   ├── constants
│   │   └── index.ts
│   ├── custom-hook
│   │   ├── index.ts
│   │   └── YourCustomHook.ts
│   ├── helpers
│   │   ├── YourHelpers.ts
│   │   └── index.ts
│   ├── interfaces
│   │   └── index.ts
│   ├── redux-global
│   │   ├── Global.action.ts
│   │   └── Global.reducers.ts
│   ├── scss
│   │   ├── Custom.scss
│   │   ├── index.scss
│   │   └── Variables.scss
│   ├── views
│   │   ├── your-views-folder
│   │   │   ├── components
│   │   │   │   ├── YourViewsComponent.scss
│   │   │   │   ├── YourViewsComponent.test.tsx
│   │   │   │   ├── YourViewsComponent.tsx
│   │   │   │   └── index.ts
│   │   │   ├── interfaces
│   │   │   │   └── index.ts
│   │   │   ├── YourViews.actions.ts
│   │   │   ├── YourViews.constants.ts
│   │   │   ├── YourViews.reducers.ts
│   │   │   ├── YourViews.sagas.ts
│   │   │   ├── YourViews.scss
│   │   │   ├── YourViews.test.tsx
│   │   │   └── YourViews.tsx
│   ├── index.ts
│   ├── react-app-env.d.ts
│   ├── serviceWorker.ts
│   └── setupTests.ts
├── .env
├── .env.example
├── .eslintignore
├── .eslintrc.js
├── .gitignore
├── .gitlab-ci.yml - - - - - - - - - - - - - - - - - - - - - - - - - # Gitlab CI/CD
├── package-lock.json
├── package.json
├── Procfile - - - - - - - - - - - - - - - - - - - - - - - - - - - - # Heroku override default command
├── README.md
└── tsconfig.json
```
