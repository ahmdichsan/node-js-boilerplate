require('dotenv').config();

const exec = require('child_process').exec;

function init() {
  try {
    const NODE_ENV = process.env.NODE_ENV;

    if (!NODE_ENV) return console.log('NODE_ENV not found');

    let isNodeEnvValid = true;
    let command = '';

    switch (NODE_ENV) {
      case 'staging':
        command = 'npm run build:staging';
        break;
      case 'production':
        command = 'npm run build:production';
        break;
    
      default:
        isNodeEnvValid = false;
        break;
    }

    if (!isNodeEnvValid) return console.log(`Unknown NODE_ENV: ${NODE_ENV}`);

    console.log('Start build...');
    exec(command, (error, stdout, stderr) => {
      if (error) return console.log(`error execute command: ${error.message}`);

      console.log('All is well!');
    });
  } catch (error) {
    const errorMessage = `error start script: ${error.message}`;
    console.log(errorMessage);
  }
};

init();
