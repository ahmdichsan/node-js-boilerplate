const fs = require('fs-extra');
require('dotenv').config();

// create compiled code directory
fs.mkdirpSync('dist');
console.log(`dist directory created`);
